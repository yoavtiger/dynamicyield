var Redis = require('ioredis');
var moment = require('moment');
var express = require('express');
var _ = require('lodash');
var redis = new Redis();
var LIST_LIMITED_SIZE = 100000;

var app = express();


/**
Beacuse we use redis I assume that the data is updated in real time and is
limited by a certain length.

I also assume that the time in the redis server and on this computer is the same.
*/


//a helper function
function sumClicksAndViews(partition){
    var clicks = _.chain(partition).pluck('clicks').
    reduce((total,n)=>total+n).value();
    var views = _.chain(partition).pluck('views').
    reduce((total,n)=>total+n).value();
    return {clicks:clicks,views:views,clicksPerViews:clicks/views}
}
app.get('/', function(req, res){
    var startTime= moment(req.query.startTime);
    var endTime = moment(req.query.endTime);
    var startOffset = startTime.diff(moment(), 'minutes');
    var endOffset = endTime.diff(moment(),'minutes') - 1;
    new Promise(function checkStartTime(resolve, reject) {
        if (startOffset>-LIST_LIMITED_SIZE){
            resolve(1);
        }else {
            reject('No data on this date');
        }
    }).then(function getClicksAndViewsFromRedis(){
        var pipeline = redis.pipeline();
        pipeline.lrange('clicksAndViews',startOffset,endOffset);
        pipeline.llen('clicksAndViews');
        return pipeline.exec();
    }).then(function checkListLength(result){
        var listLength = result[1][1];
        if (-listLength>startOffset){
            throw new Error('No data on this date');
        }
        return _.map(result[0][1], JSON.parse);
    }).then(function partitionResult(result){
        var partitions = [];
        var currentPartition = -1;
        for (var i=0; i<result.length; i++){
            if (i%40 == 0){
                currentPartition+=1;
                partitions.push([]);
            }
            partitions[currentPartition].push(result[i]);
        }
        return partitions;
    }).then(function aggregatePartitions(partitions){
        return _.map(partitions, sumClicksAndViews);
    }).then(function createSummary(partitions){
        return {
            summary:sumClicksAndViews(partitions),
            overTime:partitions};
    }).then(function addTimeData(returnJson){
        returnJson['startTime']= startTime.format('x');
        returnJson['endTime']= endTime.format('x');
        returnJson['resolution'] = endTime.diff(startTime, 'seconds');
        res.json(returnJson);
    }).catch(function (error){
        //can add a better error hanlding
        res.status(500).end();
    });
});

app.listen(3001);
